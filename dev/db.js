
const DB = ()=> {
	const dbName = '__CHR_EXT_ADD_ASSETS__'
	const url = window.location.href
	const getStorage = ()=> window.sessionStorage.getItem(dbName)
	const getJSON = ()=> JSON.parse(getStorage())
	const setStorage = (value)=> window.sessionStorage.setItem(dbName, value)
	const setJSON = (value)=> setStorage(JSON.stringify(value))

	return {
		hasDB: ()=> getStorage() ? true : false,
		delDB: ()=> window.sessionStorage.removeItem(dbName),
		hasScript: ()=> {
			const d = getJSON() || {}
			d[url] = d[url] || {}
			return d[url].script ? true : false
		},
		setScript: (value)=> {
			const d = getJSON() || {}
			d[url] = d[url] || {}
			d[url].script = value
			setJSON(d)
			return true
		},
		getScript: ()=> {
			const d = getJSON()
			return d[url].script
		},
		delScript: ()=> {
			const d = getJSON() || {}
			d[url] = d[url] || {}
			d[url].script = ''
			setJSON(d)
			return true
		}
	}
}

export default DB
