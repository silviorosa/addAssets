
chrome.tabs.onActivated.addListener(function(object,activeInfo) {
	chrome.browserAction.setIcon({path: 'icon16.png'});
})

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
	var ico = request.icon === 'active' ? 'icon16-active.png' : 'icon16.png'
	chrome.browserAction.setIcon({path: ico});
})
