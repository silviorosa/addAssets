(function() {
	const qs = (e)=> document.querySelector(e)
	const qsa = (e)=> document.querySelectorAll(e)
	qs('[name="name"]').value = 'John Smith'
	qs('[name="email"]').value = 'john@smith.com'
	qs('[name="number"]').value = '1256'
	qsa('[name="select"] option')[2].setAttribute('selected', '')
	qsa('[name="radio"]')[1].setAttribute('checked', '')
	qs('[name="check"]').setAttribute('checked', '')
}());
