
(function () {
	'use strict'

	window.tape.browserConsole = {
		passed: 0,
		failed: 0,
		hideOk: false,
		favicon: function (color) {
			var createFavicon = function (href) {
				var el = document.createElement('link');
				el.href = href;
				el.rel = 'shortcut icon';
				document.head.appendChild(el);
			};

			var insertFavicon = function (color) {
				var fav = document.querySelector('[rel*="icon"]');
				return fav ? (fav.href = color) : createFavicon(color);
			};

			var colors = {
				red: 'data:image/gif;base64,R0lGODdhEAAQAIABAMwzM////ywAAAAAEAAQAAACDoSPqcvtD6OctNqLsz4FADs=',
				gray: 'data:image/gif;base64,R0lGODlhEAAQAIABAJmZmf///yH+EUNyZWF0ZWQgd2l0aCBHSU1QACwAAAAAEAAQAAACDoSPqcvtD6OctNqLsz4FADs=',
				green: 'data:image/gif;base64,R0lGODlhEAAQAIABAADMmf///ywAAAAAEAAQAAACDoSPqcvtD6OctNqLsz4FADs'
			};

			insertFavicon(colors[color]);
		},
		setPassed: function () {
			this.passed += 1;
			if (this.failed === 0 && this.passed > 0) this.favicon('green');
		},
		setFailed: function () {
			this.failed += 1;
			if (this.failed > 0) this.favicon('red');
		},
		log: function () {
			var cl = console.log;
			var _this = this;

			console.log = function () {
				var args = Array.prototype.slice.call(arguments);

				if (typeof args[0] !== 'string') return cl.apply(null, args);

				if (args[0].indexOf('ok ') === 0) {
					_this.setPassed();
					if (_this.hideOk) return;
					return cl.apply(null, ['✔ '].concat(args));
				}

				if (args[0].indexOf('not ok ') === 0) {
					_this.setFailed();
					console.error.apply(null, ['✖ '].concat(args));
					return;
				}
				return cl.apply(null, args);
			};
		}
	};

	window.tape.browserConsole.log();
})();
