document.addEventListener('DOMContentLoaded', function() {
	const nodeFile = document.getElementById('file-upload')
	const nodeFileStr = document.getElementById('file-upload-str')
	const btn = document.getElementById('btn')
	const btnDelScript = document.getElementById('btn-del-script')
	const fileStr = nodeFileStr.textContent

	const sendMessage = function(msm) {
		chrome.tabs.query({currentWindow: true, active: true}, function(tabs) {
			chrome.tabs.sendMessage(tabs[0].id, msm)
		})
	}

	const onchange = function(ev) {
		if (this.files.length) {
			nodeFileStr.textContent = nodeFile.files[0].name
			btn.removeAttribute('disabled');
		} else {
			nodeFileStr.textContent = fileStr
			btn.setAttribute('disabled', '');
		}
	}
	nodeFile.addEventListener('change', onchange, false)

	const onclick = function(ev) {
		const file = nodeFile.files[0];

		if (file.type !== 'application/javascript') {
			sendMessage(false)
			return
		}

		if (file) {
			var reader = new FileReader();
			reader.readAsText(file, 'UTF-8');
			reader.onload = function (evt) {
				sendMessage(evt.target.result)
			}
			reader.onerror = function (evt) {
				sendMessage(false)
			}
		}
	}
	btn.addEventListener('click', onclick, false)

	const delScript = function(ev) {
		sendMessage('delScript')
	}
	btnDelScript.addEventListener('click', delScript, false)
})

