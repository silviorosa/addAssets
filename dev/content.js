
import DB from "./db";

const db = DB()

const appendScript = (script)=> {
	const node = document.getElementById('__CHR_EXT_ADD_ASSETS__SCRIPT__');
	if (node) node.remove()

	const elm = document.createElement('script')
	elm.id = '__CHR_EXT_ADD_ASSETS__SCRIPT__'
	elm.innerHTML = script
	document.body.appendChild(elm)
}

if (db.hasDB() && db.hasScript()) {
	appendScript(db.getScript())
	chrome.runtime.sendMessage({icon:'active'});
} else {
	chrome.runtime.sendMessage({icon:'default'});
}

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
	if (!request) {
		alert('ERROR FILE')
		return
	}

	if (request === 'delScript') {
		if (db.hasDB() && db.hasScript()) {
			db.delScript()
			chrome.runtime.sendMessage({icon:'default'});
			alert('SCRIPT DELETED')
			window.location.reload()
		} else {
			alert('SCRIPT NOT DELETED')
		}
		return
	}

	db.setScript(request)
	chrome.runtime.sendMessage({icon:'active'});
	alert('Script uploaded')
	window.location.reload()
})
