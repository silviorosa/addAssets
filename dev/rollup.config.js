// rollup.config.js
import babel from 'rollup-plugin-babel';
import uglify from 'rollup-plugin-uglify';
import license from 'rollup-plugin-license';
import pkg from '../package.json';


export default {
	input: './dev/content.js',
	output: {
		file: './extension/content.js',
		format: 'iife',
	},
	plugins: [
		babel({ exclude: 'node_modules/**'}),
		uglify(),
		license({ banner: getBanner() })
	]
}

function getBanner() {
	const name = pkg.nameText
	const year = new Date().getFullYear()
	const version = pkg.version
	const license = pkg.license

	return `/*!
* ${name} ${version}
* Copyright (c) ${year} Sílvio Rosa @silvior_
* ${license} license
*/
`
}
